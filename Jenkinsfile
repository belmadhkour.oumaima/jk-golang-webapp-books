pipeline {
    agent any

    environment {
        DOCKER_IMAGE = 'oma09/my-golang-app'
        IMAGE_TAG = 'latest'
        REGISTRY_CREDENTIALS_ID = 'dockerhub-login'  // Credentials ID for Docker Hub
        HEROKU_APP_NAME = 'my-golang-heroku-app1'
    }

    stages {
        stage('Setup Buildx') {
            steps {
                script {
                    // Ensure any existing buildx builder is removed and create a new one using specific Docker path
                    sh "\${DOCKER_BIN} buildx ls | grep mybuilder && \${DOCKER_BIN} buildx rm mybuilder || true"
                    sh "\${DOCKER_BIN} buildx create --name mybuilder --use"
                }
            }
        }

        stage('Build and Push Docker Image') {
            steps {
                script {
                    // Log in to Docker Hub using credentials
                    withCredentials([usernamePassword(credentialsId: REGISTRY_CREDENTIALS_ID, usernameVariable: 'DOCKERHUB_USER', passwordVariable: 'DOCKERHUB_PASS')]) {
                        sh "echo \$DOCKERHUB_PASS | \${DOCKER_BIN} login --username \$DOCKERHUB_USER --password-stdin"
                    }
                    
                    // Build and push the multi-architecture image using buildx with specific platform
                    sh "\${DOCKER_BIN} buildx build --platform linux/amd64 -t \${DOCKER_IMAGE}:\${IMAGE_TAG} --push ."
                }
            }
        }

        stage('Deploy to Heroku') {
            steps {
                script {
                    withCredentials([string(credentialsId: 'heroku_api_key', variable: 'HEROKU_API_KEY')]) {
                        sh """
                        \${DOCKER_BIN} tag \${DOCKER_IMAGE}:\${IMAGE_TAG} registry.heroku.com/\${HEROKU_APP_NAME}/web
                        echo \$HEROKU_API_KEY | \${DOCKER_BIN} login --username=_ --password-stdin registry.heroku.com
                        \${DOCKER_BIN} push registry.heroku.com/\${HEROKU_APP_NAME}/web
                        heroku container:release web --app \${HEROKU_APP_NAME}
                        """
                    }
                }
            }
        }

        stage('Check Deployment') {
            steps {
                script {
                    // Checking if the deployed application is live and accessible
                    sh "curl -s https://\${HEROKU_APP_NAME}.herokuapp.com"
                }
            }
        }
    }

    post {
        always {
            // Sending build completion email
            emailext(
                to: 'notif-jenkins@joelkoussawo.me',
                subject: "Build \${currentBuild.fullDisplayName} completed",
                body: """<html><body>
                         <p>The build of <strong>\${env.JOB_NAME}</strong> build number <strong>\${env.BUILD_NUMBER}</strong> was completed.</p>
                         <p>Status: <strong>\${currentBuild.currentResult}</strong></p>
                         <p>View build details at <a href='\${BUILD_URL}'>\${BUILD_URL}</a>.</p>
                         </body></html>""",
                mimeType: 'text/html'
            )
        }
        success {
            echo 'Build and deployment were successful!'
        }
        failure {
            echo 'Build or deployment failed.'
        }
    }
}
